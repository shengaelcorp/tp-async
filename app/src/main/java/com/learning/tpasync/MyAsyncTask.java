package com.learning.tpasync;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.TextView;

public class MyAsyncTask extends AsyncTask<Long, Long, String> {

    private Context context;
    private TextView textView;

    public MyAsyncTask(Context context, TextView textView) {
        this.context = context;
        this.textView = textView;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        this.textView.setText("started");
    }

    @Override
    protected void onProgressUpdate(Long... values) {
        super.onProgressUpdate(values);
        textView.setText(String.valueOf(values[0]));
    }

    @Override
    protected String doInBackground(Long... longs) {
        long maxValue = longs[0];

        for (long i = 0; i < maxValue; i++) {
            if (i % 10000 == 0) {
                publishProgress(i);
            }
        }
        return "Finish";
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        this.textView.setText(s);
    }

    @Override
    protected void onCancelled() {
        this.textView.setText("cancelled !");
        super.onCancelled();
    }
}
