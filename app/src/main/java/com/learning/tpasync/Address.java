package com.learning.tpasync;

public class Address {
    private String street;
    private String suite;
    private String city;
    private String zipcode;
    private Geo geo;


    // Getter Methods

    public String getStreet() {
        return street;
    }

    public String getSuite() {
        return suite;
    }

    public String getCity() {
        return city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public Geo getGeo() {
        return geo;
    }

    // Setter Methods

    public void setStreet( String street ) {
        this.street = street;
    }

    public void setSuite( String suite ) {
        this.suite = suite;
    }

    public void setCity( String city ) {
        this.city = city;
    }

    public void setZipcode( String zipcode ) {
        this.zipcode = zipcode;
    }

    public void setGeo( Geo geoObject ) {
        this.geo = geoObject;
    }

    @Override
    public String toString() {
        return "Address{" +
                "street='" + street + '\'' +
                ", suite='" + suite + '\'' +
                ", city='" + city + '\'' +
                ", zipcode='" + zipcode + '\'' +
                ", geo=" + geo +
                '}';
    }
}
