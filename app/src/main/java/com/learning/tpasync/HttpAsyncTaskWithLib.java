package com.learning.tpasync;

import android.content.Context;
import android.os.AsyncTask;
import android.util.JsonReader;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpAsyncTaskWithLib extends AsyncTask<String, Void, User[]> {

    private Context context;
    private TextView textView;

    public HttpAsyncTaskWithLib(Context context, TextView textView) {
        this.context = context;
        this.textView = textView;
    }

    @Override
    protected User[] doInBackground(String... strings) {
        String urlString = strings[0];
        try {
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10_000);
            conn.setConnectTimeout(1_000);
            conn.setRequestMethod("GET");

            conn.connect();

            int responseCode = conn.getResponseCode();
            // TODO : check response code

            InputStream inputStream = conn.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

            User[] users = new Gson().fromJson(new InputStreamReader(inputStream), User[].class);

            inputStream.close();
            conn.disconnect();

            return users;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(User[] users) {
        super.onPostExecute(users);
        textView.setText(users[0].toString());
    }
}
