package com.learning.tpasync;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    private MyAsyncTask myAsyncTask;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView counter = findViewById(R.id.counter);
        Button launchCounter = findViewById(R.id.launchCount);
        Button launchHttp = findViewById(R.id.httpButton);
        Button launchStop = findViewById(R.id.stopButton);
        launchCounter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchWithAsyncTask(counter);
            }
        });

        launchHttp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchHttpAsyncTaskWithLib(counter);
            }
        });

        launchStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myAsyncTask.cancel(false);
            }
        });


    }

    private void launchWithThread(final TextView textView) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1_000_000_000; i++) {
                    final int j = i;
                    if (i % 10000 == 0) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                textView.setText(String.valueOf(j));
                            }
                        });
                    }
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textView.setText(String.valueOf("finish !"));
                    }
                });
            }
        }).start();
    }

    private void launchWithAsyncTask(TextView textView) {
        this.myAsyncTask = new MyAsyncTask(this, textView);
        this.myAsyncTask.execute(1_000_000_000L);
    }

    private void launchHttpAsyncTask(TextView textView) {
        HttpAsyncTask httpAsyncTask = new HttpAsyncTask(this, textView);
        httpAsyncTask.execute("https://jsonplaceholder.typicode.com/users");
    }

    private  void launchHttpAsyncTaskWithLib(TextView textView) {
        HttpAsyncTaskWithLib httpAsyncTaskWithLib = new HttpAsyncTaskWithLib(this, textView);
        httpAsyncTaskWithLib.execute("https://jsonplaceholder.typicode.com/users");
    }
}
